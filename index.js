// Single line comments 
// /* Multi line comments 
// Syntax and Statements
/*
	-statements (instructions on what to perform)
	-JS stmnts usually end with semicolon (; but not required)
	
	-Syntax (set of rules that describes how statements must be constructed)
*/
console.log("Hello Batch 138!");

// Variables
/*
	-used to contain data.
	Declaring variable - set name and ready to store data
	-Syntax:

	-let/const variableName;
	-let (values that will change)
	const (values that are constant)
*/

let myVariable;

console.log(myVariable);

// console.Log(hello);

// Initializing values to variables (assigning value)

/*
	-Syntax:
	let/const variableName = value;
*/

let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;

console.log(productPrice);

/* Reassigning variable values
	-change the initial or previous value into another
*/

productName = 'Laptop';

console.log(productName);

const interest = 3.539;

// console.log(interest);

// interest = 4.489;

console.log(interest);

let supplier;

supplier = "John Smith Tradings";

console.log(supplier);

// Declaring multiple variables
let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);

// Data Types
/* Strings
	series of char that create a word
*/

let country ='Philippines'
let province = "Metro Manila"

// String concatenation
// Multiple string values can be combined to create a single string using the "+" symbol;

// Output: Metro Manila, Phillipines
let fullAddress = province + "," + country;
console.log(fullAddress)

// escape character
// \n-refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhillipines";
console.log(mailAddress)

let message = 'John\'s employees went home early';
console.log(message);

// Numeric Data Types
// Integers/Whole
let headcount = 26;
console.log(headcount)

// Decimal
let grade = 98.7;
console.log(grade)

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance)

// Combining text and strings
// Output John's grade last quarter is + 98.7
console.log("John's grade last quarter is" + grade);

// Boolean
// 1/0 or True or False
let isMarried = true;
let isGoodConduct = false;
console.log("isMaried: " + isMarried);
console.log("isGoodConduct " + isGoodConduct)

// Array
// Used to store multiple values, can store different data types but similar data types
/* Syntax
	let/const arrayName = [element0, element1, element2, ...];
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades); 

/* Objects
	special data type used to mimic real world objects/items (properties)
	Syntax:
		let/const objectName = {
			propertyA: value, 
			propertyB: value
		}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09195678910", "09992828822"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}

console.log(person);

// Null vs Undefined
/* Null
	-used to intentionally express the absence of a value in a variable declaration.
*/

let spouse = null;
let money = 0;
let myName = "";

/* Underfined
	-represents the state of a variable that has been declared but without an assigned value
*/
let fullName;

/* Functions
	- Functions in JS lines/blocks of codes that tell our device/application for perform a certain task when called/invoked
	Declaring functions
	Syntax:
	function functionName (){
		line/block of codes;
	}
*/

// Declaring a function
/*function printName(){
	console.log("My name is John");
}*/

// Invoking/Calling a function
// printName();

// Declare a function that will display your favorite animal
function Animal(){
	console.log("Cat");
}
Animal();

// (name) - parameter
// Parameter - acts as a named variable/container that exists only inside of a function
function printName(name){
	console.log("My name is " + name);
}
// Argument - the actual value that is provided a function for it to work properly
printName("John");

printName("Jane");

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);

// Finding more info about a function in the console
console.log(argumentFunction);

// Using multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log("Hello " + firstName + " " + middleName + " " + lastName)
}

createFullName("Juan", "Pablo", "Cruz");
createFullName("Juan", "Pablo");
createFullName("Juan", "Pablo", "Cruz", "Junior");

// Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// return statement
// allows the output of a function to be passed to the line/block of code
function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + lastName;
	console.log("A simple message");
}

// (initialize return statement by giving a variable name [completeName] so we can invoke)

let completeName = returnFullName(firstName, middleName, lastName);
console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName));